// GOOS=js GOARCH=wasm go1.12.13 build -o impressum.wasm main.go

package main

import "syscall/js"

func main() {

	name := "Mustermann, Max"
	adresse := "Musterstrasse 123"
	plz := "123456"
	mail := "mustermann@example.com"
	js.Global().Get("document").Call("getElementById", "name").Set("textContent", name)
	js.Global().Get("document").Call("getElementById", "adresse").Set("textContent", adresse)
	js.Global().Get("document").Call("getElementById", "plz").Set("textContent", plz)
	js.Global().Get("document").Call("getElementById", "mail").Set("textContent", mail)
	select {}
}
