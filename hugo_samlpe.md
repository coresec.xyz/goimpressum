---
title: "Impressum"
date: 2019-10-30
draft: false
---

Put "wasm_exec.js" in ./static/js/ and "impressum.wasm" in ./static/

<div>

	<h1>Angaben gem. § 5 TMG:</h1>
	<strong>Vorname, Name: </strong> <div id="name"></div> <br>
	<strong>Adresse: </strong> <div id="adresse"></div> <br>
	<strong>PLZ: </strong> <div id="plz"></div> <br>

	<h1>Kontaktaufnahme:</h1>
	<strong>E-Mail: </strong> <div id="mail"></div> <br>

</div>

<script src="../js/wasm_exec.js"></script>
<script>
	const impressum = new Go();
	WebAssembly.instantiateStreaming(fetch("../impressum.wasm"), impressum.importObject).then((result) => {
		impressum.run(result.instance);
	});
</script>